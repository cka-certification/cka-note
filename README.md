# Certified Kubernetes Administrator (CKA) Exam

## Cluster Architecture, Installation & Configuration (25%)
### Manage role based access control (RBAC).
### Use Kubeadm to install a basic cluster.
### Manage a highly-available Kubernetes cluster.
### Provision underlying infrastructure to deploy a Kubernetes cluster.
### Perform a version upgrade on a Kubernetes cluster using Kubeadm.
### Implement etcd backup and restore.

## Workloads & Scheduling (15%)
### Understand deployments and how to perform rolling update and rollbacks.
### Use ConfigMaps and Secrets to configure applications.
### Know how to scale applications.
### Understand the primitives used to create robust, self-healing, application deployments.
### Understand how resource limits can affect Pod scheduling.
### Awareness of manifest management and common templating tools.

## Services & Networking (20%)
### Understand host networking configuration on the cluster nodes.
### Understand connectivity between Pods.
### Understand ClusterIP, NodePort, LoadBalancer service types and endpoints.
### Know how to use Ingress controllers and Ingress resources.
### Know how to configure and use CoreDNS.
### Choose an appropriate container network interface plugin.

## Storage (10%)
### Understand storage classes, persistent volumes.
### Understand volume mode, access modes and reclaim policies for volumes.
### Understand persistent volume claims primitive.
### Know how to configure applications with persistent storage.

## Troubleshooting (30%)
### Evaluate cluster and node logging.
### Understand how to monitor applications.
### Manage container stdout & stderr logs.
### Troubleshoot application failure.
### Troubleshoot cluster component failure.
### Troubleshoot networking.



## What is Kubernetes?

Kubernetes lets you create, deploy, manage, and scale application containers across one or more host clusters. 

Environments running Kubernetes consist of the following key components:

-   **Kubernetes control plane**—manages Kubernetes clusters and the workloads running on them. Include components like the API Server, Scheduler, and Controller Manager.
-   **Kubernetes data plane**—machines that can run containerized workloads. Each node is managed by the kubelet, an agent that receives commands from the control plane. 
-   **Pods**—pods are the smallest unit provided by Kubernetes to manage containerized workloads.  A pod typically includes several containers, which together form a functional unit or microservice.
-   **Persistent storage**—local storage on Kubernetes nodes is ephemeral, and is deleted when a pod shuts down. This can make it difficult to run stateful applications. Kubernetes provides the Persistent Volumes (PV) mechanism, allowing containerized applications to store data beyond the lifetime of a pod or node.

## Kubernetes Architecture Overview

A Kubernetes cluster has two main components—the control plane and data plane, machines used as compute resources. 

-   **The control plane** hosts the components used to manage the Kubernetes cluster. 
-   **Worker nodes** can be virtual machines (VMs) or physical machines. A node hosts pods, which run one or more containers.

## Kubernetes Core Components: Control Plane

A control plane serves as a nerve center of each Kubernetes cluster. It includes components that can control your cluster, its state data, and its configuration. 

The Kubernetes control plane is responsible for ensuring that the Kubernetes cluster attains a desired state, defined by the user in a declarative manner. The control plane interacts with individual cluster nodes using the kubelet, an agent deployed on each node.

Here are the main components of the control plane:

### 1. kube-apiserver 

Provides an API that serves as the front end of a Kubernetes control plane. It is responsible for handling external and internal requests—determining whether a request is valid and then processing it. The API can be accessed via the kubectl command-line interface or other tools like kubeadm, and via REST calls.

### 2. kube-scheduler 

This component is responsible for scheduling pods on specific nodes according to automated workflows and user defined conditions, which can include resource requests, concerns like affinity and taints or tolerations, priority, persistent volumes (PV), and more.

### 3. kube-controller-manager 

The Kubernetes controller manager is a control loop that monitors and regulates the state of a Kubernetes cluster. It receives information about the current state of the cluster and objects within it, and sends instructions to move the cluster towards the cluster operator’s desired state. 

The controller manager is responsible for several controllers that handle various automated activities at the cluster or pod level, including replication controller, namespace controller, service accounts controller, deployment, statefulset, and daemonset.

### 4. etcd

A key-value database that contains data about your cluster state and configuration. Etcd is fault tolerant and distributed.
*etcd port 2379*

### 5. cloud-controller-manager

This component can embed cloud-specific control logic - for example, it can access the cloud provider’s load balancer service. It enables you to connect a Kubernetes cluster with the API of a cloud provider. Additionally, it helps decouple the Kuberneters cluster from components that interact with a cloud platform, so that elements inside the cluster do not need to be aware of the implementation specifics of each cloud provider.

This cloud-controller-manager runs only controllers specific to the cloud provider. It is not required for on-premises Kubernetes environments. It uses multiple, yet logically-independent, control loops that are combined into one binary, which can run as a single process. It can be used to add scale a cluster by adding more nodes on cloud VMs, and leverage cloud provider high availability and load balancing capabilities to improve resilience and performance. 


*The `kubectl` utility is a command line tool that interfaces with the Kubernetes API server to run commands against the Kubernetes cluster. The `kubectl` command is typically run on the control plane node of the cluster, although you can also use an operator node. The `kubectl` utility effectively grants full administrative rights to the cluster and all of the nodes in the cluster.



## Kubernetes Core Components: Worker Nodes

### 6. Nodes 

Nodes are physical or virtual machines that can run pods as part of a Kubernetes cluster. A cluster can scale up to 5000 nodes. To scale a cluster’s capacity, you can add more nodes.

### 7. Pods 

A pod serves as a single application instance, and is considered the smallest unit in the object model of Kubernetes. Each pod consists of one or more tightly coupled containers, and configurations that govern how containers should run. To run stateful applications, you can connect pods to persistent storage, using Kubernetes Persistent Volumes—[learn more in the following section.](https://spot.io/resources/kubernetes-architecture-11-core-components-explained/#kubernetes-persistent-volumes)

**_Learn more in our detailed guide to the_** [**_Kubernetes pod_**](https://spot.io/resources/kubernetes-architecture/kubernetes-pod-why-where-and-how/)

### 8. Container Runtime Engine

Each node comes with a container runtime engine, which is responsible for running containers. Docker is a popular container runtime engine, but Kubernetes supports other runtimes that are compliant with Open Container Initiative, including CRI-O and rkt.

### 9. kubelet 

Each node contains a kubelet, which is a small application that can communicate with the Kubernetes control plane. The kubelet is responsible for ensuring that containers specified in pod configuration are running on a specific node, and manages their lifecycle.. It executes the actions commanded by your control plane.

### 10. kube-proxy 

All compute nodes contain kube-proxy, a network proxy that facilitates Kubernetes networking services. It handles all network communications outside and inside the cluster, forwarding traffic or replying on the packet filtering layer of the operating system.

### 11. Container Networking 

Container networking enables containers to communicate with hosts or other containers. It is often achieved by using the container networking interface (CNI), which is a joint initiative by Kubernetes, Apache Mesos, Cloud Foundry, Red Hat OpenShift, and others.

CNI offers a standardized, minimal specification for network connectivity in containers. You can use the CNI plugin by passing the kubelet --network-plugin=cni command-line option. The kubelet can then read files from --cni-conf-dir and use the CNI configuration when setting up networking for each pod.



### Cluster Info

Get APi resources list and sort name\
`kubectl api-resources `

Display addresses of the master and services\
`kubectl cluster-info`

Dump current cluster state to stdout\
`kubectl cluster-info dump`

### Pod
Create an NGINX Pod\
`kubectl run nginx --image=nginx`

Generate POD Manifest YAML file (-o yaml) with (–dry-run)\
`kubectl run nginx --image=nginx --dry-run=client -o yaml`

Run with Labels, Example tier\
`kubectl run redis -l tier=db --image=redis:alpine`

Deploy a `redis` pod using the `redis:alpine` image with the labels set to `tier=db`.\
`kubectl run redis --image=redis:alpine --labels tier=db`

List all pods in all namespaces, with more details\
`kubectl get pods -o wide --all-namespaces`

List the nodes\
`kubectl get nodes`

Use "kubectl describe" for related events and troubleshooting\
`kubectl describe pods <podid>`

Use "kubectl explain" to check the structure of a resource object.\
`kubectl explain deployment --recursive`

Add "-o wide" in order to use wide output, which gives you more details.\
`kubectl get pods -o wide`

Check always all namespaces by including "--all-namespaces"\
`kubectl get pods --all-namespaces`

Start a hazelcast pod and let the container expose port 5701.\ 
`kubectl run hazelcast --image=hazelcast/hazelcast --port=5701`

Start a hazelcast pod and set environment variables "DNS_DOMAIN=cluster" and "POD_NAMESPACE=default" in thecontainer.\
`kubectl run hazelcast --image=hazelcast/hazelcast --env="DNS_DOMAIN=cluster" --env="POD_NAMESPACE=default"`

Start a hazelcast pod and set labels "app=hazelcast" and "env=prod" in the container.\
`kubectl run hazelcast --image=hazelcast/hazelcast --labels="app=hazelcast,env=prod"

Dry run. Print the corresponding API objects without creating them.\  
`kubectl run nginx --image=nginx --dry-run=client`

Start a nginx pod, but overload the spec with a partial set of values parsed from JSON.\
`kubectl run nginx --image=nginx --overrides='{ "apiVersion": "v1", "spec": { ... } }

Start a busybox pod and keep it in the foreground, don't restart it if it exits.\
`kubectl run -i -t busybox --image=busybox --restart=Never`

Start the nginx pod using the default command, but use custom arguments (arg1 .. argN) for that command.\
`kubectl run nginx --image=nginx -- <arg1> <arg2> ... <argN>

Start the nginx pod using a different command and custom arguments.\
`kubectl run nginx --image=nginx --command -- <cmd> <arg1> ... <argN>


### ReplicaSets

how many replication controler exist on the system\
`kubectl get rc`

How about now? How many ReplicaSets do you see?\
`kubectl get replicasets`


How many PODs are DESIRED in the `new-replica-set`?\
`kubectl get rs`

What is the image used to create the pods in the `new-replica-set`?\
` kubectl describe rs new-replica-se`

Scale the ReplicaSet to 5 PODs.\
```
kubectl edit replicaset new-replica-set
kubectl scale rs new-replica-set --replicas=5
```
Now scale the ReplicaSet down to 2 PODs.\
`kubectl scale rs new-replica-set --replicas=2`


### Deployment
Example Deployment File (dep-nginx.yaml) using NGINX\

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
	name: nginx-deployment
	labels:
	    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.21.6
        ports:
        - containerPort: 80 
```
### What is a Kubernetes Deployment?
A Kubernetes Deployment tells Kubernetes how to create or modify instances of the pods that hold a containerized application. Deployments can help to efficiently scale the number of replica pods, enable the rollout of updated code in a controlled manner, or roll back to an earlier deployment version if necessary.

Create Deployment\
`kubectl create -f dep-nginx.yaml`

Get Deployments\
`kubectl get deployments`

Update Deployment\
`kubectl edit deployment.v1.apps/nginx-deployment`

See rollout status\
`kubectl rollout status deployment.v1.apps/nginx-deployment`

Describe Deployment\
`kubectl describe deployment`

Rolling back to a previous revision\
`kubectl rollout undo deployment.v1.apps/nginx-deployment`

Create a deployment\
`kubectl create deployment --image=nginx nginx`

Generate Deployment YAML file (-o yaml). Don’t create it(–dry-run)\
`kubectl create deployment --image=nginx nginx --dry-run=client -o yaml`

Generate Deployment YAML file (-o yaml). Don’t create it(–dry-run) with 4 Replicas (–replicas=4)\
`kubectl create deployment --image=nginx nginx --dry-run=client -o yaml > nginx-deployment.yaml`

Save it to a file, make necessary changes to the file (for example, adding more replicas) and then create the deployment.\
`kubectl create -f nginx-deployment.yaml`
OR
In k8s version 1.19+, we can specify the –replicas option to create a deployment with 4 replicas.\
```
kubectl create deployment webapp --image kodekloud/webapp-color --replicas=3
kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml
```
Increase replicas number for nginx-deployment\
`kubectl scale deployment/nginx-deployment --replicas=5`

Using autoscaling\
`kubectl autoscale deployment/nginx-deployment --min=2 --max=5`

### Kubernetes Autoscaling: How to use the Kubernetes Autoscaler

Imagine a situation where when user load increases or decreases on an application, there is no way for the application to scale out or in its resources as required.\
Kubernetes Autoscaling mechanisms help scale in and out pods and nodes as required. There are **Three** different methods supported by Kubernetes Autoscaling.
- Horizontal Pod Autoscaler (HPA): Horizontal scaling refers to the deployment of extra pods in the Kubernetes Cluster in response to a growing load.
- Vertical Pod Autoscaler (VPA): The Vertical Pod Autoscaler is an autoscaling tool that can be used to resize pods for optimal CPU and memory resources.
- Cluster Autoscaler (CA): Based on the presence of pending pods and node utilization data, the Kubernetes Autoscaler increases or decreases the size of a Kubernetes cluster by adding or removing nodes.\
When the Kubernetes Autoscaler identifies pending pods that cannot be scheduled, potentially resulting in resource constraints, it adds nodes to the cluster. Moreover, when the utilization of a node falls below a particular threshold set by the cluster administrator, it removes nodes from the cluster allowing for all the pods to have a place to run and avoiding unnecessary nodes. 

More details [Autoscaling](https://www.clickittech.com/devops/kubernetes-autoscaling/)



### Service

-   **Port** exposes the Kubernetes service on the specified port within the cluster. Other pods within the cluster can communicate with this server on the specified port.
-   **TargetPort** is the port on which the service will send requests to, that your pod will be listening on. Your application in the container will need to be listening on this port also.
-   **NodePort** exposes a service externally to the cluster by means of the target nodes IP address and the NodePort. NodePort is the default setting if the port field is not specified.

Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379\
```
kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml
kubectl expose pod redis --type=ClusterIP --port=6379 --name=redis-service --dry-run=client -o yaml
```
(This will automatically use the pod’s labels as selectors)\
Or\
This will not use the pods labels as selectors, instead it will assume selectors as app=redis. You cannot pass in selectors as an option. So it does not work very well if your pod has a different label set. So generate the file and modify the selectors before creating the service.\
`kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml`

Create a Service named nginx of type NodePort to expose pod nginx’s port 80 on port 30080 on the nodes:\
This will automatically use the pod’s labels as selectors, but you cannot specify the node port. You have to generate a definition file and then add the node port in manually before creating the service with the pod.\
`kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml`
Or
`kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml`

Use the following command to create the YAML for the service named `web-service`\
```
kubectl expose deployment/webapp --port=80 --target-port=80 --type=NodePort -n web --dry-run -o yaml > web-service.yaml
```

Expose the `hr-web-app` as service `hr-web-app-service` application on port `30082` on the nodes on the cluster. The web application listens on port `8080`.

``
### Name Space

How many Namespaces exist on the system?\
`kubectl get namespace --no-headers | wc -l`

How many pods exist in the `research` namespace?\
`kubectl get pods --namespace=research`

How to switch namespace in kubernetes\
`kubectl config set-context --current --namespace=my-namespace`

Create Name Space dev-ns\
`kubectl create ns dev-ns`

Create deployment with dev-ns Namespace\	
`kubectl create deployment redis-deploy --image=redis --replicas=2 -n=dev-ns`

List all pods in all namespaces, with more details\
`kubectl get pods -o wide --all-namespaces`

List all pods in all namespaces, with more details\
`kubectl get pods -o wide -n dev-ns`



##  Workloads & Scheduling (15%)
To see the status of `scheduler` pod\
`kubectl get pods --namespace kube-system`

Manually schedule the pod on `node01`. Delete and recreate the POD if necessary.\
``` shell
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  nodeName: node01
  containers:
  -  image: nginx
     name: nginx
```
Delete old pod and recreate pod  assign Pod on node01\
`kubectl delete pod nginx`
`kubectl create -f nginx.yaml`

#### Labels and Selectors
_Labels_ are key/value pairs that are attached to objects, such as pods. Labels are intended to be used to specify identifying attributes of objects that are meaningful and relevant to users, but do not directly imply semantics to the core system. Labels can be used to organize and to select subsets of objects.\
 Each object can have a set of key/value labels defined. Each Key must be unique for a given object.\

```json
"metadata": {
  "labels": {
    "key1" : "value1",
    "key2" : "value2"
  }
}
```

## Label selectors[](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#label-selectors)

Unlike _names and UIDs_ labels do not provide uniqueness. In general, we expect many objects to carry the same label(s).
Via a _label selector_, the client/user can identify a set of objects. The label selector is the core grouping primitive in Kubernetes.
The API currently supports two types of selectors: **equality-based_ and _set-based**. A label selector can be made of multiple _requirements_ which are comma-separated. In the case of multiple requirements, all must be satisfied so the comma separator acts as a logical _AND_ (`&&`) operator.

**Equality-based_ requirement**[](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#equality-based-requirement)
_Equality-_ or _inequality-based_ requirements allow filtering by label keys and values. Matching objects must satisfy all of the specified label constraints, though they may have additional labels as well. Three kinds of operators are admitted `=`,`==`,`!=`. The first two represent _equality_ (and are synonyms), while the latter represents _inequality_. For example:
```
environment = production
tier != frontend
```

**Set-based_ requirement**[](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#set-based-requirement)
_Set-based_ label requirements allow filtering keys according to a set of values. Three kinds of operators are supported: `in`,`notin` and `exists` (only the key identifier). For example:

```
environment in (production, qa)
tier notin (frontend, backend)
partition
!partition
```


The `get` subcommand can be used to display a pod's labels\
`kubectl get pods --show-labels`

Find PODs are in the `finance` business unit (`bu`)\
`kubectl get pods --selector bu=finance`

How many objects are in the `prod` environment including PODs, ReplicaSets and any other objects?\
`kubectl get all --selector env=prod`
`kubectl get all --selector env=prod --no-headers | wc -l`

Identify the POD which is part of the `prod` environment, the `finance` BU and of `frontend` tier?\
`kubectl get all --selector env=prod,bu=finance,tier=frontend`


### Taints and Tolerations

### Node taints

Taint is a **_property of a node(applied to nodes only)_** that allows you to repel a set of pods unless those pods explicitly tolerate the node taint.\  
In simple words, if we apply taints to a node it will create a restriction shield around the node which will prevent the pods to schedule inside that node.\

Taint Effects
There are three type’s of taint effect which we can apply to a node and
1- NoSchedule
If we apply this taint effect to a node then it will only allow the pods which have a toleration effect equal to NoSchedule. But if a pod is already scheduled in a node and then you apply taint to the node having effect NoSchedule, then the pod will remain scheduled in the node.

2- PreferNoSchedule
In this effect, it will first prefer for no scheduling of pod but if you have a single node and a PreferNoSchedule taint is applied on it. Then even if the pod didn’t tolerate the taint it will get schedule inside the node which has a taint effect: PreferNoSchedule.

3- NoExecute
This effect will not only restrict the pod to get scheduled in the node but also if a pod is already scheduled in a specific node and we have applied a taint of effect NoExecute to the specific node, it will immediately throw

Create a taint on `node01` with key of `spray`, value of `mortein` and effect of `NoSchedule`\
`kubectl taint nodes <node_name> key=value:effect`\
`kubectl taint nodes node01 taintkey1=tainvlaue1:NoSchedule`\
`kubectl taint nodes node01 spray=mortin:NoSchedule`\

`kubectl describe nodes node01 | grep -i taint`\

```
apiVersion: apps/v1
kind: Deployment
metadata:
	name: nginx-deployment
	labels:
	    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.21.6
        ports:
        - containerPort: 80 
      tolerations:
	  - key: "spray" 
		operator: "Equal"
		vlaue: "mortin"
		effect: "NoSchedule"
```

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: bee
  name: bee
spec:
  containers:
  - image: nginx
    name: bee
  tolerations:
  - key: 'spray'
    operator: "Equal"
    value: "mortein"
    effect: "NoSchedule"
```

If you take a look at the above deployment you will see tolerations block inside podSpec and inside that you will find some keywords like:  
**1 — key** : The value which you have specified while applying node taint.  
**2 — value** : Value that you have mentioned while applying the node taint.  
**3 — effect** : Effect that you have mentioned while applying the node taint.  
**4 — Operator** : There are 2 values of operator Equal and Exists.
_Equal: If we specify operator as Equal, then we have to specify all the key , value, and effect option.  
Exists: If we specify operator as Exists then it’s not compulsory to mention key, value, and effect option._

`kubectl create -f prod-deployment.yaml`

Which node is the POD `mosquito` on now? execute & look at the `Node` column.\
`kubectl get pod -o wide`

Use the following command to verify the pods have been scheduled\
`kubectl get pods -o wide`

Untaints
`kubectl taint nodes node01 spray=mortin:NoSchedule-`

Remove the taint on `controlplane`, which currently has the taint effect of `NoSchedule`
`kubectl taint nodes controlplane node-role.kubernetes.io/control-plane:NoSchedule-\


Let us first have a look at the already running pods on the different nodes\
`kubectl get pods -o wide`


### Assign Pods to Nodes in Kubernetes Using nodeSelector and Affinity Features

These scenarios are addressed by a number of primitives in Kubernetes:

-   `nodeSelector` — This is a simple Pod scheduling feature that allows scheduling a Pod onto a node whose labels match the `nodeSelector` labels specified by the user.
-   `node Affinity` — This is the enhanced version of the `nodeSelector` introduced in Kubernetes 1.4 in beta. It offers a more expressive syntax for fine-grained control of how Pods are scheduled to specific nodes.
-   `Inter-Pod Affinity` — This feature addresses the third scenario above. Inter-Pod affinity allows co-location by scheduling Pods onto nodes that already have specific Pods running.

#### nodeSelector
As we’ve already mentioned, `nodeSelector` is the early Kubernetes feature designed for manual Pod scheduling. The basic idea behind the `nodeSelector` is to allow a Pod to be scheduled only on those nodes that have label(s) identical to the label(s) defined in the `nodeSelector`. The latter are key-value pairs that can be specified inside the `PodSpec`.

Find what nodes exist in your cluster\
`kubectl get nodes --show-labels`

Add a new label with the key `disktype` and value `ssd` to the `host02` node\
`kubectl label nodes node01 disktype=ssd`

Apply a label `color=blue` to node `node01`\
`kubectl label node node01 clor=blue`

Below command worked for me to remove label\
`kubectl label node <nodename> <key>-`
`kubectl label node worker1 system-`
`kubectl label node node01 c0lor-`

How many Labels exist on node node01?\
`kubectl describe node node01`

```bash
apiVersion: v1  
kind: Pod  
metadata:  
	name: httpd  
	labels:  
		env: prod  
spec:  
	containers:  
	- name: httpd  
	  image: httpd  
    nodeSelector:  
	    disktype: ssd
```
`kubectl create -f test-pod.yaml`

`httpd` Pod will be scheduled on the node with the `disktype=ssd` label\

`kubectl get pods -o wide`


### nodeAffinity
**Node affinity** is similar to nodeSelector. It allows us to constrain which nodes our pod is eligible to be scheduled on, based on labels on the node.

There are currently two types of node affinity [NodeAffinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) :

1.  requiredDuringSchedulingIgnoredDuringExecution (must)
2.  preferredDuringSchedulingIgnoredDuringExecution (not guaranteed)

With the "IgnoredDuringExecution" part of the names makes the pod continues to run on the node even if labels on a node change at runtime such that the affinity rules on a pod are no longer met.

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent 
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: disktype
            operator: In
            values:
            - ssd
```
As we can see the **operator** **In** being used in the manifest. This node affinity syntax supports the following operators: In, NotIn, Exists, DoesNotExist, Gt, Lt
We can use NotIn and DoesNotExist to achieve node **anti-affinity** behavior, or use **node taints** to **repel** pods from specific nodes.

`kubectl get nodes --show-labels | grep ssd`

Let's apply the manifest to create a Pod that is scheduled onto our chosen node\
`kubectl apply -f pod-nginx-required-affinity.yaml`
`kubectl get pods -o wide`

Pod affinity supports the following operations:
-   In
-   NotIn
-   Exists
-   DoesNotExist

_If node doesn’t even have the label set. So we don’t really have to even check the value of the label, as long as we are sure we don’t set the label size to the smaller nodes, using the “**Exist**” operator will give us the same result as “NotIn” in our case.

```yaml
piVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.kubernetes.io/control-plane
                operator: Exists
```

[nodeAffinity](https://www.bogotobogo.com/DevOps/Docker/Docker_Kubernetes_nodeSelector_nodeAffinity_taints_tolerations_podAffinity_antiAffinity.php)

[Pod Affinity](https://technos.medium.com/node-affinity-in-kubernetes-320cfce0898e)

**Taints and Tolerations vs Node Affinity**


### Resource
Another pod called `elephant` has been deployed in the default namespace. It fails to get to a running state. Inspect this pod and identify the `Reason` why it is not running.
`kubectl describe pod elephant | grep -A5 State:`


### Daemon Sets
DaemonSets are used to ensure that some or all of your K8S nodes run a copy of a pod, which allows you to run a daemon on every node.
DaemonSets are used in Kubernetes when you need to run one or more pods on all (or a subset of) the nodes in a cluster. The typical use case for a DaemonSet is logging and monitoring for the hosts. For example, a node needs a service (daemon) that collects health or log data and pushes them to a central system or database (like ELK stack). DaemonSets can be deployed to specific nodes either by the nodes’ user-defined labels or using values provided by Kubernetes like the node hostname.

How many `DaemonSets` are created in the cluster in all namespaces?\
`kubectl get daemonsets --all-namespaces`

On how many nodes are the pods scheduled by the **DaemonSet** `kube-proxy`\
`kubectl describe daemonset kube-proxy --namespace=kube-system`

What is the image used by the POD deployed by the `kube-flannel-ds` **DaemonSet**?\
`kubectl describe daemonset kube-flannel-ds --namespace=kube-system`

Deploy a **DaemonSet** for `FluentD` Logging.
Use the given specifications.
```
kubectl create deployment elasticsearch --image=k8s.gcr.io/fluentd-elasticsearch:1.20 -n kube-system --dry-run=client -o yaml > fluentd.yaml
```
ext, remove the replicas, strategy and status fields from the YAML file using a text editor. Also, change the kind from `Deployment` to `DaemonSet`.


### Create static Pods
_Static Pods_ are managed directly by the kubelet daemon on a specific node, without the [API server](https://kubernetes.io/docs/concepts/overview/components/#kube-apiserver) observing them. 

#### What are static pods?
-   managed directly by kubelet daemon on a specific node
-   static Pods running on a node are visible on the API server, but cannot be controlled from there.
-   Unlike Pods that are managed by the control plane (for example, a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)); instead, the kubelet watches each static Pod (and restarts it if it fails).
-   The name of static pod is suffixed with the name of the node
-   The `spec` of a static Pod cannot refer to other API objects (e.g., [ServiceAccount](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/), [ConfigMap](https://kubernetes.io/docs/concepts/configuration/configmap/), [Secret](https://kubernetes.io/docs/concepts/configuration/secret/), etc).
- 
#### What is the path of the directory holding the static pod definition files?
The static pod definition files path can be found in the config files of the kubelet daemon on a specific node.

-e for shows all the current processes, -f shows full format listing, 
search for --config, which shows the config files for the kubelet\
`ps -ef | grep kubelet`

Result looks something like this`\
``--config=/var/lib/kubelet/config.yaml`

Search for static inside the config file\  
`grep -i static /var/lib/kubelet/config.yaml`

Result is like this\   
`staticPodPath: /etc/kubernetes/manifests_`

Run this command on the node where the kubelet is running\  
`systemctl restart kubelet`

#### How to create a static pod?
ssh into the right node\  
`ssh node-master`

ssh using the internal IP address if the node is not found\  
`kubectl node node-01 -o wide`  
`ssh 172.17.0.41`

create a yaml file for the static pod image=busybox\ 
`kr busy-box --image=busybox --restart=Never --dry-run=client -o yaml > bb.yaml`

put the yaml file in the right path, refer to the above question\  
`mv bb.yaml /etc/kubernetes/manifests/bb.yaml`

Create a static pod named `static-busybox` that uses the `busybox` image and the command `sleep 1000`\
`kubectl run static-busybox --image=busybox  --dry-run=client -o yaml --command -- sleep 1000 > static.yaml`


`Create a static pod named `static-busybox` that uses the `busybox` image and the command `sleep 1000`\

Check the static pods status\  
`docker ps`

See the mirror Pod on the API server\  
`kubectl pods -A`

How many static pods exist in this cluster in all namespaces?\
`kubectl get pod --all-namespaces`
look for those with `-controlplane` appended in the name\

Create a static pod named `static-busybox` that uses the `busybox` image and the command `sleep 1000`\
```
kubectl run --restart=Never --image=busybox static-busybox --dry-run=client -o yaml --command -- sleep 1000 > static-busybox.yaml
```

We just created a new static pod named **static-greenbox**. Find it and delete it.
This question is a bit tricky. But if you use the knowledge you gained in the previous questions in this lab, you should be able to find the answer to it.
`kubectl get pods --all-namespaces -o wide | grep static-greenbox`

From the result of this command, we can see that the pod is running on node01.  Next, SSH to `node01` and identify the path configured for static pods in this
```
ssh node01
ps -ef | grep /usr/bin/kubelet
grep -i staticpod /var/lib/kubelet/config.yaml
```

Here the staticPodPath is `/etc/just-to-mess-with-you`  
Navigate to this directory and delete the YAML file:
```
cd /etc/just-to-mess-with-you
ls greenbox.yaml
rm -rf greenbox.yaml
```
Exit out of node01 using `CTRL + D` or type `exit`. You should return to the `controlplane` node. Check if the `static-greenbox` pod has been deleted:
`kubectl get pods --all-namespaces -o wide | grep static-greenbox`


### Configuring Scheduler Profiles

#### Logging and Monitoring
Identify the node that consumes the `most` CPU\
`kubectl top node --sort-by='cpu' --no-headers | head -1`
`kubectl top node --sort-by='cpu' --no-headers | head -2`

Identify the node that consumes the `most` Memory\
`kubectl top node --sort-by='memory' --no-headers | head -1`

Identify the POD that consumes the `most` Memory\
`kubectl top pod --sort-by='memory' --no-headers | head -1`

Identify the POD that consumes the `least` CPU\
`kubectl top pod --sort-by='cpu' --no-headers | tail -1`

Managing Application Logs


## Application lifecycle management

#### Rolling Updates and Rollbacks
nspect the deployment and identify the number of PODs deployed by it\
`kubectl get pod`
`kubectl get deployment`

What container image is used to deploy the applications?\
`kubectl describe deployment frontend`
`kubectl describe pod frontend`

Inspect the deployment and identify the current strategy\
`kubectl describe deployment frontend`

Upgrade the application by setting the image on the deployment to `kodekloud/webapp-color:v2`\
`kubectl edit deployment frontend` 

Change the deployment strategy to `Recreate` Only update the strategy type for the existing deployment. **strategy type: Recreate**
`kubectl edit deployment frontend`

```
apiVersion: apps/v1 
kind: Deployment 
metadata: 
	name: frontend 
	namespace: default 
spec: 
	replicas: 4 
	selector: 
		matchLabels: 
		name: webapp 
	strategy: 
		type: Recreate 
	template: 
		metadata: 
			labels: 
				name: webapp 
		spec: 
			containers: 
			-	image: kodekloud/webapp-color:v2 
				name: simple-webapp 
				ports: 
				-	containerPort: 8080 
					protocol: TCP
```

Upgrade the application by setting the image on the deployment to `kodekloud/webapp-color:v3`\
`kubectl set image deployment/frontend simple-webapp=kodekloud/webapp-color:v3`

#### Configure Applications

Configuring applications comprises of understanding the following concepts:
-   Configuring Command and Arguments on applications
-   Configuring Environment Variables
-   Configuring Secrets

RUN vs CMD vs ENTRYPOINT
-   `RUN` executes commands and creates new image layers. The _run_ instruction executes when we build the image. That means the command passed to _run_ executes on top of the current image in a new layer.

-   `CMD` sets the command and its parameters to be executed by default after the container is started. With the _cmd_ instruction, we can specify a default command that executes when the container is starting.

-   `ENTRYPOINT` configures the command to run when the container starts, similar to `CMD` from a functionality perspective. The parameters in `ENTRYPOINT` are always used, while the extra parameters of `CMD` can be dynamically replaced when the container starts.

#### command and args in Kubernetes

```
piVersion: v1
kind: Pod 
metadata:
  name: webapp-green
  labels:
      name: webapp-green
spec:
  containers:
  - name: simple-webapp
    image: kodekloud/webapp-color
    command: ["python", "app.py"]
    args: ["--color", "pink"]
```

Create a pod with the ubuntu image to run a container to sleep for 5000 seconds.
`kubectl run --image=nginx ubuntu-sleeper-2 --restart=Never --command sleef 500 --dry-run=client -o yaml`

Create a pod with the ubuntu image to run a container to sleep for 5000 seconds. Modify the file ubuntu-sleeper-2.yaml.
Pod Name: ubuntu-sleeper-2
Command: sleep 5000
```bash
apiVersion: v1
kind: Pod 
metadata:
  name: ubuntu-sleeper-2
spec:
  containers:
  - name: ubuntu
    image: ubuntu
    command:
      - "sleep"
      - '5000'
~             
```

Create a pod with the given specifications. By default it displays a `blue` background. Set the given command line arguments to change it to `green`.
```bash
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: webapp-green
  name: webapp-green
spec:
  containers:
  - image: kodekloud/webapp-color
    name: webapp-green
    args: ["--color", "green"]                          
```

#### Configure Environment Variables in Applications

#### What is configMap
ConfigMaps are APIs that store configuration data in key-value pairs. Their primary function is to keep the configuration separate from the container image. It can represent the entire configuration file or individual properties.

**Create configMap using Imperative way**
```bash
kubectl create configmap \  
configmap-name --from-literal=key=value
```

```bash
kubectl create configmap \
app-config --from-literal=db_host=https://database.example.com
```

```bash
kubectl create configmap \
literal-example --from-literal=City=Hamburg \
--from-literal=Country=Germany \
--from-literal=username=Ann-Arbor \
--fromliteral=Position=Director \
--from-literal=Department=Admin
```

Create a new ConfigMap for the webapp-color POD. Use the spec given below.
ConfigName Name: webapp-config-map
Data: APP_COLOR=darkblue

```bash
apiVersion: v1
kind: ConfigMap
metadata:
  name: webapp-config-map
data:
  APP_COLOR: darkblue
```

Update the environment variable on the POD to use the newly created ConfigMap
Pod Name: webapp-color
EnvFrom: webapp-config-map
```bash
apiVersion: v1
kind: Pod
metadata:
  labels:
    name: webapp-color
  name: webapp-color
  namespace: default
spec:
  containers:
  - envFrom:
    - configMapRef:
         name: webapp-config-map
    image: kodekloud/webapp-color
    name: webapp-color
```

#### What Are Kubernetes Secrets?
A Kubernetes secret is an object storing sensitive pieces of data such as usernames, passwords, tokens, and keys. Secrets are created by the system during an app installation or by users whenever they need to store sensitive information and make it available to a pod.

#### **Creating a Secret via kubectl**
To create a secret via kubectl, you’re going to want to first create text file to store the contents of your secret, in this case a username.txt and password.txt:

How many secrets are in the default namespace\
`kubectl get secrets`

How many secrets are defined in the `dashboard-token` secret?\
`kubectl describe secrets dashboard-token`

The reason the application is failed is because we have not created the secrets yet. Create a new secret named db-secret with the data given below.
Secret Name: db-secret
Secret 1: DB_Host=sql01
Secret 2: DB_User=root
Secret 3: DB_Password=password123
```bash
kubectl create secret generic db-secret --from-literal=DB_Host=sql01 --from-literal=DB_User=root --from-literal=DB_Password=password123
```

Configure `webapp-pod` to load environment variables from the newly created secret.
Delete and recreate the pod if required.
Pod name: webapp-pod
Image name: kodekloud/simple-webapp-mysql
Env From: Secret=db-secret
```bash
apiVersion: v1 
kind: Pod 
metadata:
  labels:
    name: webapp-pod
  name: webapp-pod
  namespace: default 
spec:
  containers:
  - image: kodekloud/simple-webapp-mysql
    imagePullPolicy: Always
    name: webapp
    envFrom:
    - secretRef:
        name: db-secret
```

Store the credentials in files with the values encoded in base64
```bash
echo -n 'admin' > ./username.txt
echo -n '1f2d1e2e67df' > ./password.txt
```

convert the strings to `base64` as follows:
`echo -n 'admin' | base64 `

[Managing Secrets using kubectl] (<https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/>)


#### Multi Container PODs

The application outputs logs to the file /log/app.log. View the logs
`kubectl -n elastic-stack exec -it app -- cat /log/app.log`

### Cluster Maintenance – Section Introduction

#### OS Upgrades

We need to take `node01` out for maintenance. Empty the node of all applications and mark it unschedulable.
`kubectl drain node01 --ignore-daemonsets`

(<https://stackoverflow.com/questions/48078196/remove-daemonset-pod-from-a-node>)

The maintenance tasks have been completed. Configure the node `node01` to be schedulable again. 
`kubectl uncordon node01`

**Running the `uncordon` command on a node will not automatically schedule pods on the node. When new pods are created, they will be placed on node01.**

***cordon Vs drain***
'drain' waits for graceful termination. You should not operate on the machine until the command completes.

`cordon` Mark node as unschedulable.
Examples:  
Mark node “foo” as unschedulable.  
kubectl cordon foo


#### Cluster Upgrade Introduction

Inspect the applications and taints set on the nodes.
`kubectl describe nodes controlplane | grep -i taint`


How many nodes can host workloads in this cluster?
`kubectl describe nodes controlplane | grep -i taint`

Upgrade the `controlplane` components to exact version `v1.25.0`
Upgrade the kubeadm tool (if not already), then the controlplane components, and finally the kubelet. Practice referring to the Kubernetes documentation page.  
**Note:** While upgrading kubelet, if you hit dependency issues while running the `apt-get upgrade kubelet` command, use the `apt install kubelet=1.25.0-00` command instead.
```
kubectl drain controlplane --ignore-daemonsets
kubectl cordon controlplane 
apt-mark unhold kubeadm
apt-get update && apt-get install -y kubeadm=1.25.0-00
apt-mark hold kubeadm
kubeadm version
kubeadm upgrade plan
sudo kubeadm upgrade apply v1.25.0
apt install kubelet=1.25.0-00
sudo systemctl daemon-reload
sudo systemctl restart kubelet
kubectl uncordon controlplane 
```


Next is the worker node. `Drain` the worker node of the workloads and mark it `UnSchedulable`
```
kubectl drain node01 --ignore-daemonsets
kubectl cordon node01
```

Upgrade the worker node to the exact version `v1.25.0`
```
 apt-mark unhold kubeadm 
 apt-get update && apt-get install -y kubeadm=1.25.0-00
 apt-mark hold kubeadm
 sudo kubeadm upgrade node
 apt-mark unhold kubelet kubectl
 sudo kubeadm upgrade node
 apt-get install kubelet=1.25.0-00 
 systemctl daemon-reload
 systemctl restart kubelet
 kubeadm version
```

#### Backup and Restore

etcd backu with location
```
ETCDCTL_API=3 etcdctl \
  --endpoints=https://127.0.0.1:2379 \
  --cacert=/etc/kubernetes/pki/etcd/ca.crt \
  --cert=/etc/kubernetes/pki/etcd/server.crt \
  --key=/etc/kubernetes/pki/etcd/server.key \
  snapshot save /opt/backup/etcd.db
```
resore etcd backup
```
service kube-apiserver stop
ETCDCTL_API=3 etcdctl snapshot restore /opt/backup/etcd.db
```
 verify snapshot
 `ETCDCTL_API=3 etcdctl --write-out=table snapshot status /opt/backup/etcd.db`


What is the version of ETCD running on the cluster?
`kubectl describe pod etcd-controlplane -n kube-system`

At what address can you reach the ETCD cluster from the controlplane node?
`kubectl -n kube-system describe pod etcd-controlplane | grep '\--listen-client-urls'

Where is the ETCD server certificate file located?
`kubectl -n kube-system describe pod etcd-controlplane | grep '\--cert-file'`

Where is the ETCD CA Certificate file located?
`kubectl -n kube-system describe pod etcd-controlplane | grep '\--trusted-ca-file'`

Take a snapshot of the **ETCD** database using the built-in **snapshot** functionality.
```bash
ETCDCTL_API=3 etcdctl --endpoints=https://[127.0.0.1]:2379 \ 
--cacert=/etc/kubernetes/pki/etcd/ca.crt \ 
--cert=/etc/kubernetes/pki/etcd/server.crt \ 
--key=/etc/kubernetes/pki/etcd/server.key \ 
snapshot save /opt/snapshot-pre-boot.db
```


```bash
ETCDCTL_API=3 etcdctl snapshot restore --data-dir /var/lib/etcd-data-back /opt/snapshot-pre-boot.db
```
Next, update the `/etc/kubernetes/manifests/etcd.yaml`:
We have now restored the etcd snapshot to a new path on the controlplane - `/var/lib/etcd-data-back`
example-
```bash
volumes:
  - hostPath:
      path: /var/lib/etcd-data-back
      type: DirectoryOrCreate
    name: etcd-data
```

[ ETCD Backup and Resote ] (<https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/#backing-up-an-etcd-cluster>)


How many `clusters` are defined in the kubeconfig on the `student-node`?
`kubectl config get-clusters`

How many nodes (both controlplane and worker) are part of `cluster1`?
Make sure to switch the context to `cluster1`:

```bash
kubectl config use-context cluster1
kubectl get nodes
```

What is the default data directory used the for `ETCD` datastore used in `cluster1`?

```bash
kubectl config use-context cluster1
kubectl get pod -n kube-system
kubectl -n kube-system describe pod etcd-cluster1-controlplane | grep data-dir
```

You can inspect the process on the `controlplane` node on `cluster2` as shown below:
`ssh cluster2-controlplane ps -ef | grep etcd`

What is the default data directory used the for `ETCD` datastore used in `cluster1`?
```
kubectl get pod -A
kubectl -n kube-system describe pod etcd-cluster1-controlplane | grep data-dir
```

### Security

### Kubernetes Security Primitives

The Kubernetes API is the front end of the Kubernetes control plane and is how users interact with their Kubernetes cluster. The API (application programming interface) server determines if a request is valid and then processes it.

In essence, the API is the interface used to manage, create, and configure Kubernetes clusters. It's how the users, external components, and parts of your cluster all communicate with each other.

At the center of the Kubernetes control plane is the API server and the HTTP API that it exposes, allowing you to query and manipulate the state of Kubernetes objects.

## Creating RSA Keys using OpenSSL

```custom
# generate a private key with the correct length
openssl genrsa -out private-key.pem 3072

# generate corresponding public key
openssl rsa -in private-key.pem -pubout -out public-key.pem

# optional: create a self-signed certificate
openssl req -new -x509 -key private-key.pem -out cert.pem -days 360

# optional: convert pem to pfx
openssl pkcs12 -export -inkey private-key.pem -in cert.pem -out cert.pfx
```

https://www.scottbrady91.com/openssl/creating-rsa-keys-using-openssl

Check certificate expiration
`kubeadm certs check-expiration`

What is the Common Name (CN) configured on the Kube API Server Certificate?
**penSSL Syntax:** `openssl x509 -in file-path.crt -text -noout`
`openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text`

What is the Common Name (CN) configured on the ETCD Server certificate?
`openssl x509 -in /etc/kubernetes/pki/etcd/server.crt -text`

print CSR in single line 
`cat akshay.csr | base64 -w 0`

What groups is this CSR requesting access to?
`kubectl get csr agent-smith ent-smit -o yaml`

Reject that CSR request.
`kubectl certificate deny agent-smith`

Let's get rid of it. Delete the new CSR object
`kubectl delete csr agent-smith`


https://www.youtube.com/watch?v=OHmgb7h-2-g


### kubeconfig

I would like to use the `dev-user` to access `test-cluster-1`. Set the current context to the right one so I can do that.
`kubectl config use-context research --kubeconfig my-kube-config`

We don't want to have to specify the kubeconfig file option on each command. Make the `my-kube-config` file the default kubeconfig.
`cp my-kube-config ~/.kube/config`

What is the current context set to in the `my-kube-config` file?
`kubectl config current-context --kubeconfig my-kube-config`

I would like to use the `dev-user` to access `test-cluster-1`. Set the current context to the right one so I can do that.
Once the right context is identified, use the `kubectl config use-context` command.
`kubectl config --kubeconfig=/root/my-kube-config use-context research`


### API Groups in Kubernetes
API groups make it easier to extend the Kubernetes API. The API group is specified in a REST path and in the apiVersion field of a serialized object.

There are several API groups in Kubernetes:

The core (also called legacy) group is found at REST path /api/v1. The core group is not specified as part of the apiVersion field, for example, apiVersion: v1.

The named groups are at REST path /apis/$GROUP_NAME/$VERSION and use apiVersion: $GROUP_NAME/$VERSION (for example, apiVersion: batch/v1). You can find the full list of supported API groups in the Kubernetes API reference.
For more details [API Group](https://www.waytoeasylearn.com/learn/api-groups-in-kubernetes/)

### Kubernetes Authorization
The role of Kubernetes authorization is important, While authentication refers to validating the identity of a specific subject to decide whether or not it could be granted access, authorization handles what follows that access. Using authorization mechanisms, you can fine-tune who has access to which resources on your Kubernetes cluster. In this article, we will be discussing Role-Based Access Control ([RBAC](https://www.weave.works/blog/kubernetes-rbac-101)) and how you can use it to secure your cluster.

## How Do Kubernetes Authorize Requests?

Kubernetes uses the API server to authorize requests against a set of policies. It’s worth noting that the authorization is a step that comes after the authentication is successful. The workflow goes as follows:

1.  The user is authenticated to the API server using one of the supported authentication methods. For more information about this process, please refer to our article about [Kubernetes](https://www.weave.works/blog/kubernetes-devops-the-perfect-match) Authentication.
    
2.  Assuming that the request was aiming to retrieve a list of pods in the namespace kube-system (for example, using kubectl get pods -n kube-system). First, the user is authenticated in step 1, then the credentials are passed together with the _verb_, the _resource_ (and other aspects) that the user is trying to execute to the authorization module.
    
3.  If the user (by a _user_ we refer to a human user as well as applications) is authorized to execute the request, it is passed on to the admission controller. Otherwise, the API server replies with a 403 Forbidden HTTP status code.
## Introduction to RBAC

Role-Based Access Control (RBAC for short) has been part of Kubernetes since version 1.8. You’re strongly encouraged to use it even in non-production environments. There are some important terms that you should know when dealing with RBAC:

-   **The Entity:** this is a subject that needs to access a _resource_ on the cluster. The Entity could be you, one of your colleagues, a pod, or an external program that you need to grant programmatic access to the cluster.
-   **The Resource:** the object that needs to be accessed. For example, a pod, a [configMap](https://www.weave.works/blog/prometheus-configmaps-continuous-deployment/), a [Secret](https://www.weave.works/blog/kubernetes-secrets-101), etc.
-   **The Role:** since it is inefficient to grant each user a specific set of permissions, and perhaps replicate them to multiple users who need the same access level, it’s better to create a role with that set of permissions. Multiple roles can belong to a user, and multiple users can belong to a single role. So, instead of manually removing permissions from a user account, you can just remove it from the role.
-   **The Role Binding:** this is where the actual link between the role and the users (Entities in RBAC terminology) who will belong to this role is made.

Once you create a role, you need to define which actions the Entity can execute. They can be classified into:

-   **Read-only:** where the entity cannot modify the resource. The verbs get and list
-   **Read-write:** where the entity can modify the resource. The verbs that fall into this category are create, update, delete, and delete collections.

[Kubernetes Authorization](https://www.weave.works/blog/kubernetes-authorization)

### Role Based Access Controls
Inspect the environment and identify the authorization modes configured on the cluster.
`ps -ef | grep authorization`
`cat /etc/kubernetes/manifests/kube-apiserver.yaml `

How many roles exist in the `default` namespace?
`kubectl get roles`
`kubectl get roles --all-namespaces`
`kubectl get roles -A`


How many roles exist in all namespaces together?
`kubectl get roles -A --no-headers | wc -l`

What are the resources the `kube-proxy` role in the `kube-system` namespace is given access to?
`kubectl describe role kube-proxy -n kube-system`

What actions can the `kube-proxy` role perform on `configmaps`?
``

Which account is the `kube-proxy` role assigned to?
```
kubectl get rolebindings -n kube-system
kubectl describe rolebinding kube-proxy -n kube-system
```

A user `dev-user` is created. User's details have been added to the `kubeconfig` file.  Check if the user can list pods in the `default` namespace.
`kubectl get pods --as dev-user`

Create the necessary roles and role bindings required for the `dev-user` to create, list and delete pods in the `default` namespace.

Use the given spec:
-   Role: developer
-   Role Resources: pods
-   Role Actions: list
-   Role Actions: create
-   Role Actions: delete
-   RoleBinding: dev-user-binding
-   RoleBinding: Bound to dev-user

```
kubectl create role --help
kubectl create role developer --verb=create,delete --verb=list --verb=watch --resource=pods

kubectl create rolebinding --help
kubectl create rolebinding dev-user-binding --clusterrole=developer --user=dev-user

kubectl describe rolebinding dev-user-binding
```

A set of new roles and role-bindings are created in the `blue` namespace for the `dev-user`. However, the `dev-user` is unable to get details of the `dark-blue-app` pod in the `blue` namespace. Investigate and fix the issue.
We have created the required roles and rolebindings, but something seems to be wrong.

```
kubectl get pod dark-blue-app -n blue --as dev-user
kubectl get role -n blue
kubectl describe role developer -n blue
kubectl edit role developer -n blue

```

Add a new rule in the existing role `developer` to grant the `dev-user` permissions to create deployments in the `blue` namespace.
Remember to add api group `"apps"`.

Lets try to create a deployment as dev-user
`kubectl create deployment nginx --image=nginx -n blue --as dev-user`
```
kubectl create deployment nginx --image=nginx -n blue --as dev-user
kubectl describe role developer -n blue
kubectl create deployment nginx --image=nginx -n blue --as dev-user
```

[kubectl-auth-can-i](https://www.mankier.com/1/kubectl-auth-can-i)
Check to see if I can create pods in any namespace
`kubectl auth can-i create pods --all-namespaces`
  
Check to see if I can list deployments in my current namespace
`kubectl auth can-i list deployments.apps`
  
Check to see if I can do everything in my current namespace ("*" means all)
`kubectl auth can-i '*' '*'`
  
 Check to see if I can get the job named "bar" in namespace "foo"
`kubectl auth can-i list jobs.batch/bar -n foo`
  
Check to see if I can read pod logs
`kubectl auth can-i get pods --subresource=log`
  
Check to see if I can access the URL /logs/
`kubectl auth can-i get /logs/`
  
List all allowed actions in namespace "foo"
`kubectl auth can-i --list --namespace=foo`

`kubectl auth can-i create pods --as dev-user`
`kubectl auth can-i create pods --as dev-user --namespace prod`

### Cluster Roles

How many `ClusterRoles` do you see defined in the cluster?
`kubectl get clusterrole --no-headers | wc -l`

How many `ClusterRoleBindings` exist on the cluster?
`kubectl get clusterrolebindings --no-headers | wc -l`

What user/groups are the `cluster-admin` role bound to?
`kubectl describe clusterrolebinding cluster-admin`

What level of permission does the `cluster-admin` role grant? Inspect the `cluster-admin` role's privileges.

`kubectl describe clusterrole cluster-admin`

A new user `michelle` joined the team. She will be focusing on the `nodes` in the cluster. Create the required `ClusterRoles` and `ClusterRoleBindings` so she gets access to the `nodes`.

[Role and RoleBinding](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)

```
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: node-admin
rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["get", "watch", "list", "create", "delete"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: read-secrets-global
subjects:
- kind: User
  name: michelle # Name is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: node-admin
  apiGroup: rbac.authorization.k8s.io
~                                      
```


### Servie Account
How many Service Accounts exist in the default namespace?
`kubectl get serviceaccount`

What is the secret token used by the default service account?
`kubectl describe serviceaccount default`

We just deployed the Dashboard application. Inspect the deployment. What is the image used by the deployment?
```
kubectl get deployment
kubectl describe deployment web-dashboard
```

Inspect the Dashboard Application POD and identify the Service Account mounted on it.
```
kubectl get pod
kubectl get pod web-dashboard-68f98dc77c-wmwht -o yaml
```

At what location is the ServiceAccount credentials available within the pod?
*See the volume mount path*
```
kubectl get pod
kubectl get pod web-dashboard-68f98dc77c-wmwht -o yaml
```

Create a new ServiceAccount named `dashboard-sa`
`kubectl create serviceaccount dashboard-sa`

Create an authorization token for the newly created *dashboard-sa* service account, copy the generated token and paste it into the `token` field of the UI
`kubectl create token dashboard-sa`

Edit the deployment to change ServiceAccount from `default` to `dashboard-sa`
-   Deployment name: web-dashboard
-   Service Account: dashboard-s    
-   Deployment Ready
`kubectl set serviceaccount deploy/web-dashboard dashboard-sa`
or
`kubectl edit deployment web-dashboard -n default`

`kubectl set serviceaccount --help`

Set deployment nginx-deployment's service account to serviceaccount1
`kubectl set serviceaccount deployment nginx-deployment serviceaccount1`


### Image Security

What secret type must we choose for `docker registry`?
`kubectl create secret --help`

We decided to use a modified version of the application from an internal private registry. Update the image of the deployment to use a new image from `myprivateregistry.com:5000`
`image: myprivateregistry.com:5000/nginx:alpine`

Create a secret object with the credentials required to access the registry.
Name: `private-reg-cred`  
Username: `dock_user`  
Password: `dock_password`  
Server: `myprivateregistry.com:5000`  
Email: `dock_user@myprivateregistry.com`

CheckCompleteIncomplete
-   Secret: private-reg-cred
-   Secret Type: docker-registry
-   Secret Data
`kubectl create secret docker-registry private-reg-cred --docker-username=dock_user --docker-password=dock_password --docker-server=myprivateregistry.com:5000 --docker-email=dock_user@myprivateregistry.com`

Create a Pod that uses your Secret[](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/)
```
apiVersion: v1
kind: Pod
metadata:
  name: private-reg
spec:
  containers:
  - name: private-reg-container
    image: <your-private-image>
  imagePullSecrets:
  - name: regcred
```


### Security Contexts
What is the user used to execute the sleep process within the `ubuntu-sleeper` pod?
`kubectl exec ubuntu-sleeper -- whoami`

Edit the pod `ubuntu-sleeper` to run the sleep process with user ID `1010`.
add securityContext in pod spect
**securityContext:
	runAsUser: 1010**

```
`kubectl edit pod ubuntu-sleeper`
```

``` shell
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: "2022-12-10T06:03:37Z"
  name: ubuntu-sleeper
  namespace: default
  resourceVersion: "884"
  uid: f5474db7-59b4-4649-9d28-1bd83ad91810
spec:
  containers:
  - command:
    - sleep
    - "4800"
    image: ubuntu
    imagePullPolicy: Always
    name: ubuntu
    resources: {}
    securityContext:
      runAsUser: 1010
```
`kubectl replace --force -f /tmp/kubectl-edit-3759346400.yaml`


### Network Policies

#### Ingress and egress
From the point of view of a Kubernetes pod, **ingress** is incoming traffic to the pod, and **egress** is outgoing traffic from the pod. 
In Kubernetes network policy, you create ingress and egress “allow” rules independently (egress, ingress, or both).


`kubectl get networkpolicy`
or
`kubectl get netpol`

Which pod is the Network Policy applied on?
`kubectl describe netpol payroll-policy`

What type of traffic is this Network Policy configured to handle?
`kubectl describe netpol payroll-policy`

Create a network policy to allow traffic from the `Internal` application only to the `payroll-service` and `db-service`.
Use the spec given below. You might want to enable ingress traffic to the pod to test your rules in the UI.
-   Policy Name: internal-policy
-   Policy Type: Egress
-   Egress Allow: payroll
-   Payroll Port: 8080
-   Egress Allow: mysql
-   MySQL Port: 3306
```bash
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: internal-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
            name: internal
  policyTypes:
    - Ingress
    - Egress
  egress:
    - to:
        - podSelector:
            matchLabels:
              name: payroll
      ports:
        - protocol: TCP
          port: 8080
    - to:
        - podSelector:
            matchLabels:
              name: mysql
      ports:
        - protocol: TCP
          port: 3306
```


### Storage in Docker


### Volume Driver Plugins in Docker

### Container Storage Interface (CSI)

### What Is a Container Network Interface (CNI)?

### Persistent Volume 

The application stores logs at location `/log/app.log`. View the logs.
You can exec in to the container and open the file:  
`kubectl exec webapp -- cat /log/app.log`


### Persistent Volume Claims

Configure a volume to store these logs at `/var/log/webapp` on the host.
Use the spec provided below.
-   Name: webapp
-   Image Name: kodekloud/event-simulator
-   Volume HostPath: /var/log/webapp
-   Volume Mount: /log
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: webapp
  name: webapp
spec:
  containers:
  - image: kodekloud/event-simulator
    name: webapp
    volumeMounts:
    - mountPath: /log
      name: web-log
  volumes:
    - name: web-log
      hostPath:
        path: /var/log/webapp
        type: Directory
```
[For details about Volume please click here ](https://kubernetes.io/docs/concepts/storage/volumes/)

Create a `Persistent Volume` with the given specification.
CheckCompleteIncomplete
-   Volume Name: pv-log
-   Storage: 100Mi
-   Access Modes ReadWriteMany
-   Host Path: /pv/log
-   Reclaim Policy: Retain

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-log
spec:
  capacity:
    storage: 100Mi
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: /pv/log
```

Let us claim some of that storage for our application. Create a `Persistent Volume Claim` with the given specification.

CheckCompleteIncompleteNext
-   Volume Name: claim-log-1
-   Storage Request: 50Mi
-   Access Modes: ReadWriteOnce
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: claim-log-1
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Block
  resources:
    requests:
      storage: 50Mi
```

[For more details about Persistent Volume Claim](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistent-volume-claim-requesting-a-raw-block-volume)

Why is the claim not bound to the available `Persistent Volume`?
`kubectl get pv`
`kubectl get pvc`
Look under the `Access Modes` section. The Access Modes set on the PV and the PVC do not match.

---
Update the `webapp` pod to use the persistent volume claim as its storage.
Replace `hostPath` configured earlier with the newly created `PersistentVolumeClaim`.

CheckCompleteIncompleteNext
-   Name: webapp
-   Image Name: kodekloud/event-simulator
-   Volume: PersistentVolumeClaim=claim-log-1
-   Volume Mount: /log
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: weapp
  name: webapp
spec:
  containers:
  - image: kodekloud/event-simulator
    name: webapp
    volumeMounts:
    - mountPath: /log
      name: log-vol
  volumes:
    - name: log-vol
      persistentVolumeClaim:
        claimName: claim-log-1
```

---

### Storage Class
How many `StorageClasses` exist in the cluster right now?
`kubectl get sc `

What is the name of the `Storage Class` that does not support `dynamic` volume provisioning?
`kubectl get sc `

Let's fix that. Create a new `PersistentVolumeClaim` by the name of `local-pvc` that should bind to the volume `local-pv`.
Inspect the pv `local-pv` for the specs.
CheckCompleteIncompleteNext
-   PVC: local-pvc
-   Correct Access Mode?
-   Correct StorageClass Used?
-   PVC requests volume size = 500Mi?
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: local-pvc
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: local-storage
  resources:
    requests:
      storage: 500Mi
```

---

### Networking

Linux Network Namespace?
*Linux network namespaces are a Linux kernel feature allowing us to isolate network environments through virtualization. For example, using network namespaces, you can create separate network interfaces and routing tables that are isolated from the rest of the system and operate independently.
Linux includes 6 types of namespaces: pid, net, uts, mnt, ipc, and user.

### Our first task is to create a new pair of `veth` (virtual Ethernet devices) interfaces, *veth0 and veth1*, by using the *ip link add* command

```
# create the pair of veth interfaces named, veth0 and veth1
sudo ip link add veth0 type veth peer name veth1

# confirm that veth0 is created
sudo ip link show veth0

# confirm that veth1 is created
sudo ip link show veth1
```

```
# create the vnet0 network namespace
sudo ip netns add vnet0

#check Namespace
sudo ip netns

# assign the veth0 interface to the vnet0 network namespace
sudo ip link set veth0 netns vnet0

# assign the 10.0.1.0/24 IP address range to the veth0 interface
sudo ip -n vnet0 addr add 10.0.1.0/24 dev veth0

# bring up the veth0 interface
sudo ip -n vnet0 link set veth0 up

# bring up the lo interface, because packets destined for 10.0.1.0/24
# (like ping) goes through the "local" route table
sudo ip -n vnet0 link set lo up

# confirm that the interfaces are up
sudo ip -n vnet0 addr show
```

What happens if we try to `ping`the `veth0` interface from both the host and `vnet0` network namespaces?
Notice that `veth0` is no longer reachable from the host network namespace.
```
# veth0 no longer appears in the host network namespace
sudo ip link show veth0

# ping doesn't work from the host network namespace
sudo ping -c10 10.0.1.0

# ping works from inside the vnet0 network namespace
sudo ip netns exec vnet0 ping -c10 10.0.1.0
```


### Configure the 2nd Network Namespace
```
# create the vnet1 network namespace
sudo ip netns add vnet1

# assign the veth1 interface to the vnet1 network namespace
sudo ip link set veth1 netns vnet1

# assign the 10.0.2.0/24 IP address range to the veth1 interface
sudo ip -n vnet1 addr add 10.0.2.0/24 dev veth1

# bring up the veth0 interface
sudo ip -n vnet1 link set veth1 up

# bring up the lo interface, because packets destined for 10.0.2.0/24
# (like ping) goes through the "local" route table
sudo ip -n vnet1 link set veth1 up

# confirm that the interfaces are up
sudo ip -n vnet1 addr show
```


Similar to the `veth0` interface, the `veth1` interface is no longer reachable from the host network namespace. `ping` only works from within the `vnet1` network namespace:
```
sudo ip link show veth1

ping -c10 10.0.2.0

sudo ip netns exec vnet1 ping -c10 10.0.2.0
```

### Configure The Routes  Between The Subnets
However, we can’t `ping` either of the `veth` pairs from their peer network namespace 😟😟
```
sudo ip netns exec vnet1 ping -c10 10.0.1.0
connect: Network is unreachable

sudo ip netns exec vnet0 ping -c10 10.0.2.0
connect: Network is unreachable
```

Let’s examine the route tables in both network namespaces:
```
sudo ip -n vnet0 route
10.0.1.0/24 dev veth0 proto kernel scope link src 10.0.1.0

sudo ip -n vnet1 route
10.0.2.0/24 dev veth1 proto kernel scope link src 10.0.2.0
```

They have no routes to other subnets. We can insert new route entries into the route tables using the `ip route add` command:

```
# update the vnet0 route table by adding a route to 10.0.2.0/24
sudo ip -n vnet0 route add 10.0.2.0/24 dev veth0

# confirm that packets destined for 10.0.2.0/24 is routed to veth0
sudo ip -n vnet0 route get 10.0.2.0

10.0.2.0 dev veth0 src 10.0.1.0


# update the vnet1 route table by adding a route to 10.0.1.0/24
sudo ip -n vnet1 route add 10.0.1.0/24 dev veth1

# confirm that packets destined for 10.0.1.0/24 is routed to veth1
sudo ip -n vnet1 route get 10.0.1.0

10.0.1.0 dev veth1 src 10.0.2.0
```

If we tried to `ping` the `veth` interfaces again from the peer network namespace…
`ip netns exec vnet0 ping -c10 10.0.2.0`

... it works!! 🎉🎉🎉

We can also use tcpdump to capture the packets transmitted between the two network namespaces:
`ip netns exec vnet0 tcpdump -i veth0 icmp -l`

[More Details about Network Namespace](https://itnext.io/create-your-own-network-namespace-90aaebc745d)

### CNI

How many nodes are part of this cluster?
`kubectl get nodes`

What is the Internal IP address of the `controlplane` node in this cluster?
`kubectl get node -o wide`

What is the network interface configured for cluster connectivity on the `controlplane` node?
```
# see the IP address assigned to the `controlplane` node.
kubectl get nodes -o wide

#In this case, the internal IP address used for node for node to node communication.
#Here you can see that the interface associated with this IP is `eth0` on the host.
ip a | grep 10.8.20.6
or
ip a | grep -B2 10.58.27.12
```


What is the MAC address assigned to `node01`?
SSH to node01 and run following command
`ip link show eth0`

We use `Containerd` as our container runtime. What is the interface/bridge created by `Containerd` on this host?
`ip a`

What is the state of the interface `cni0`?
`ip link`

If you were to ping google from the `controlplane` node, which route does it take
What is the IP address of the Default Gateway?
`ip route show default`

What is the port the `kube-scheduler` is listening on in the `controlplane` node?
`netstat -nplt | grep scheduler`
or
`netstat -tulpn | grep kube-scheduler`
or
`cat /etc/kubernetes/manifests/kube-scheduler.yaml`

Notice that ETCD is listening on two ports. Which of these have more client connections established?
`netstat -nplt | grep etcd`


### Pod Networking


### CNI in kubernetes

Inspect the kubelet service and identify the container runtime value is set for Kubernetes.
`ps -aux | grep kubelet | grep --color container-runtime`

What is the path configured with all binaries of CNI supported plugins?
The CNI binaries are located under /opt/cni/bin by default.

Identify which of the below plugins is not available in the list of available CNI plugins on this host?
`ls /opt/cni/bin/`

What is the CNI plugin configured to be used on this kubernetes cluster?
`ls /etc/cni/net.d/`

What is the POD IP address range configured by `weave`?
`ip addr show weave`

What is the default gateway configured on the PODs scheduled on `node01`?
SSH to the `node01` by running the command: 
`ssh node01` 
and 
`ip route` 


### Service Networking
Kubernetes [services](http://kubernetes.io/docs/concepts/services-networking/service/) provide a way of abstracting access to a group of pods as a network service. The group of pods is usually defined using a [label selector](http://kubernetes.io/docs/concepts/overview/working-with-objects/labels). Within the cluster, the network service is usually represented as a virtual IP address, and kube-proxy load balances connections to the virtual IP across the group of pods backing the service.

The virtual IP is discoverable through Kubernetes DNS. The DNS name and virtual IP address remain constant for the lifetime of the service, even though the pods backing the service may be created or destroyed, and the number of pods backing the service may change over time.

Kubernetes services can also define how a service is accessed from outside of the cluster, using one of the following:

-   A node port, where the service can be accessed via a specific port on every node
-   A load balancer, where a network load balancer provides a virtual IP address that the service can be accessed via from outside the cluster

What network range are the nodes in the cluster part of?
```
ip a | grep eth0
ipcalc -b 10.17.213.255
```
What is the range of IP addresses configured for PODs on this cluster?
```
kubectl get pod -n kube-system
kubectl logs weave-net-ngps5 weave -n kube-system
```

What is the IP Range configured for the services within the cluster?
`cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep cluster-ip-range`

How many kube-proxy pods are deployed in this cluster?
`kubectl get pod -n kube-system | grep kube-proxy`

What type of proxy is the kube-proxy configured to use?
`kubectl logs kube-proxy-bm5c7 -n kube-system`

How does this Kubernetes cluster ensure that a kube-proxy pod runs on all nodes in the cluster? Inspect the kube-proxy pods and try to identify how they are deployed. daemonsets
`kubectl -n kube-system get ds`

### DNS in kubernetes

Identify the DNS solution implemented in this cluster.
`kubectl get pods -n kube-system`

What is the name of the service created for accessing CoreDNS?
`kubectl get service -n kube-system`

What is the IP of the CoreDNS server that should be configured on PODs to resolve services?
`kubectl get service -n kube-system`

Where is the configuration file located for configuring the CoreDNS service?
```
kubectl get pod -n kube-system
kubectl describe pod coredns-6d4b75cb6d-d5ljk -n kube-system
```

How is the Corefile passed in to the CoreDNS POD?
`kubectl get pod coredns-6d4b75cb6d-d5ljk -n kube-system -o yaml`

What is the name of the ConfigMap object created for Corefile?
`kubectl get pod coredns-6d4b75cb6d-d5ljk -n kube-system -o yaml`

What is the root domain/zone configured for this kubernetes cluster?
```
kubectl get configmaps -n kube-system 
kubectl get configmaps coredns -n kube-system -o yaml
```


### CoreDNS in Kubernetes


### Ingress Controller

Which namespace is the `Ingress Controller` deployed in?
`kubectl get all -A`

What is the name of the Ingress Controller Deployment?
`kubectl get deployment -A`

Which namespace are the applications deployed in?
`kubectl get pod -A`

How many applications are deployed in the `app-space` namespace?
`kubectl get deployment -n app-space`

Which namespace is the Ingress Resource deployed in?
`kubectl get ingress -A`

What is the name of the Ingress Resource?
`kubectl get ingress --all-namespaces`

What is the `Host` configured on the `Ingress Resource`?
look at `Host` under the `Rules` section.
`kubectl describe ingress -n app-space`

What backend is the `/wear` path on the Ingress configured with?
look under the `Rules` section.
`kubectl describe ingress --namespace app-space`

If the requirement does not match any of the configured paths what service are the requests forwarded to?
look at the `Default` backend field.
`kubectl describe ingress --namespace app-space`

You are requested to change the URLs at which the applications are made available.

Make the video application available at `/stream`.
`kubectl edit ingress -n app-space`

You are requested to add a new path to your ingress to make the food delivery application available to your customers.
Make the new application available at `/eat`.
`kubectl edit ingress --namespace app-space`

You are requested to make the new application available at `/pay`.
Identify and implement the best approach to making this application available on the ingress controller and test to make sure its working. Look into annotations: rewrite-target as well.

```
#know the service and port details.
kubectl get svc -n critical-space
```

Let us now deploy an Ingress Controller. First, create a namespace called `ingress-space`.
We will isolate all ingress related objects into its own namespace.
`kubectl create namespace ingress-space`

The NGINX Ingress Controller requires a ConfigMap object. Create a ConfigMap object in the `ingress-space`. Use the spec given below. No data needs to be configured in the ConfigMap.
`kubectl create configmap nginx-configuration --namespace ingress-space`

The NGINX Ingress Controller requires a ServiceAccount. Create a ServiceAccount in the `ingress-space` namespace. Use the spec provided below.
`kubectl create serviceaccount ingress-serviceaccount --namespace ingress-space`

### Troubleshooting

How to chnge default Name Space
`kubectl config set-context --current --namespace=alpha`


Scale the deployment `app` to 2 pods.
`kubectl scale deployment app --replicas=2 -n default`


### JSON

Get the list of nodes in JSON format and store it in a file at `/opt/outputs/nodes.json`.
`kubectl get nodes -o json > /opt/outputs/nodes.json`

Get the details of the node `node01` in json format and store it in the file `/opt/outputs/node01.json`.
`kubectl get node node01 -o json > /opt/outputs/node01.json`

Use JSON PATH query to fetch node names and store them in `/opt/outputs/node_names.txt`. Remember the file should only have node names.
`kubectl get nodes -o=jsonpath='{.items[*].metadata.name}' > /opt/outputs/node_names.txt`

A kube-config file is present at `/root/my-kube-config`. Get the user names from it and store it in a file `/opt/outputs/users.txt`.
Use the command `kubectl config view --kubeconfig=/root/my-kube-config` to view the custom kube-config.
`kubectl config view --kubeconfig=/root/my-kube-config -o jsonpath='{.users[*].name}' > /opt/outputs/users.txt`

A set of Persistent Volumes are available. Sort them based on their capacity and store the result in the file `/opt/outputs/storage-capacity-sorted.txt`.
`kubectl get pv --sort-by=.spec.capacity.storage > /opt/outputs/storage-capacity-sorted.txt`

That was good, but we don't need all the extra details. Retrieve just the first 2 columns of output and store it in `/opt/outputs/pv-and-capacity-sorted.txt`.
The columns should be named `NAME` and `CAPACITY`. Use the `custom-columns` option and remember, it should still be sorted as in the previous question.
`kubectl get pv --sort-by=.spec.capacity.storage -o=custom-columns=NAME:.metadata.name,CAPACITY:.spec.capacity.storage > /opt/outputs/pv-and-capacity-sorted.txt`

Use a JSON PATH query to identify the context configured for the `aws-user` in the `my-kube-config` context file and store the result in `/opt/outputs/aws-context-name`.
`kubectl config view --kubeconfig=my-kube-config -o jsonpath="{.contexts[?(@.context.user=='aws-user')].name}" > /opt/outputs/aws-context-name`

